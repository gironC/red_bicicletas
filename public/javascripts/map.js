var mymap = L.map('mapid').setView([14.8492909, -91.5194832], 15);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZ2lyb25jIiwiYSI6ImNraG81eGlyMjA5N2cycm8xc3N3dW11ZTEifQ.lBfUOcI65j1BQUQEFyxqqQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

async function solicitud() {
    const soli = await fetch('api/bicicletas');
    const bicis = await soli.json();
    return bicis;
}
solicitud().then((res)=> {
    res.bicicletas.map(function (item){
        L.marker(item.ubicacion,{title: item.id}).addTo(mymap);
    });
})