var express = require('express');
var router = express.Router();

var bicicletaCotroller = require('../controllers/bicicletaController');

router.get('/',bicicletaCotroller.bicicleta_list);
router.get('/create',bicicletaCotroller.bicicleta_create_get);
router.post('/create',bicicletaCotroller.bicicleta_create_post);
router.post('/delete/:id',bicicletaCotroller.bicicleta_delete);

module.exports = router;