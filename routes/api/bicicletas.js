var express = require('express');
var router = express.Router();

var bicicletaCotroller = require('../../controllers/api/bicicletaControllerApi');

router.get('/',bicicletaCotroller.bicicleta_list);
router.post('/create',bicicletaCotroller.bicicleta_create);
router.delete('/delete',bicicletaCotroller.bicicleta_delete);
router.put('/update',bicicletaCotroller.bicicleta_update);

module.exports = router;