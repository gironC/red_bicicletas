var Bicicleta = require('../../models/bicicletaModel');

beforeEach(() => { Bicicleta.allBicis = []});

describe('Bicicleta.allBicis', () => {
  it('comienza vacio', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe('Bicicleta.add', () => {
  it('agregar bicicleta', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1,'rojo','urbana',[14.8492909,-91.5194832]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe('Bicicleta.findById', () => {
  it('devuelve la bici con id 1', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1,'rojo','montaña',[14.8492909,-91.5194832]);
    Bicicleta.add(a);
    var b = new Bicicleta(2,'blanco','urbana',[14.8492909,-91.5194832]);
    Bicicleta.add(b);
    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(a.color)
  });
});

describe('Bicicleta.removeById', () => {
  it('devuelve 1 luego 0', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    var a = new Bicicleta(1,'rojo','montaña',[14.8492909,-91.5194832]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    Bicicleta.removeById(1);
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});