var Bicicleta = require('../../models/bicicletaModel');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', () => {
  describe('GET bicicletas /', () => {
    it('Status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      var a = new Bicicleta(1,'rojo','urbana',[14.8492909,-91.5194832]);
      Bicicleta.add(a);

      request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
        expect(response.statusCode).toBe(200);
      });
    });
  });
  describe('POST bicicletas /create', () => {
    it('Status 200', (done) => {
      var header = {"content-type": "application/json"};
      var aBici = '{"id": 2, "color": "negro", "modelo": "de casa", "lat": 14.8492909, "lng": -91.5194832}';
      request.post({
        headers: header,
        url: 'http://localhost:3000/api/bicicletas/create',
        body: aBici
      }, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        expect(Bicicleta.findById(2).color).toBe('negro');
        done();
      })
    });
  });
});